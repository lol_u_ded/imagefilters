package at.jku.tk.mms.img.filters;

import java.awt.Color;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.FilteredImageSource;
import java.awt.image.RGBImageFilter;
import java.io.File;
import java.util.Properties;
import at.jku.tk.mms.img.FilterInterface;
import at.jku.tk.mms.img.pixels.Pixel;

/** Just to show correct handles in ComboBox */
public class GreyScaleFilter implements FilterInterface{


	@Override
	public Image runFilter(BufferedImage image, Properties settings) {
		BufferedImage greyscale = new BufferedImage(image.getColorModel(), image.copyData(null), image.isAlphaPremultiplied(),null);
		
		int height = image.getHeight();
		int width = image.getWidth();
		
		// for loop that iterates through each pixel of the image
		for (int runH = 0; runH<height; runH++) {
			
			for(int runW = 0; runW<width; runW++) {
		
				Color c = new Color(image.getRGB(runW,runH));
				
				// greyscale addition
				int red = (int) (c.getRed() * 0.299);
				int green =(int) (c.getGreen() * 0.587);
				int blue = (int) (c.getBlue() *0.114);
		
				int grey = red+green+blue;
				
				//set new color & create the grey image
				Color newColor = new Color(grey,grey,grey);
				greyscale.setRGB(runW,runH,newColor.getRGB());
			}
		}
		
		return greyscale;
	}

	@Override
	public String[] mandatoryProperties() {
		return new String[] { };
	}

	@Override
	public String toString() {
		return "Grey Filter";
	}

}
