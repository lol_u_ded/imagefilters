package at.jku.tk.mms.img.filters;

import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.util.Properties;

import at.jku.tk.mms.img.FilterInterface;

/** Perform sub sampling on the image */
public class Subsampling implements FilterInterface {

	@Override
	public Image runFilter(BufferedImage image, Properties settings) {
		int rate = Integer.parseInt(settings.getProperty("rate"));
		int widthImage = image.getWidth();
		BufferedImage bi = new BufferedImage(widthImage, image.getHeight(), BufferedImage.TYPE_INT_ARGB);
		
		int heightBi = bi.getHeight();
		int widthBi = bi.getWidth();
		int xCoord = 0;
		int yCoord = 0;

		// run through all coordinates of bi
		while (yCoord < heightBi) {
			while (xCoord < widthBi) {

				int rgbX = image.getRGB(xCoord, yCoord);

				// Set subsampling for xCoord
				for (int i = xCoord; i < xCoord + rate; i++) {
					for (int j = yCoord; j < yCoord + rate; j++) {
						if (i < widthImage && j < heightBi)
							bi.setRGB(i, j, rgbX);
					}
				}
				// get to next xCoord jump over sampled Coords
				xCoord = xCoord + rate;
			}

			// reset xCoord for yCoord
			xCoord = 0;
			int rgbY = image.getRGB(xCoord, yCoord);

			// Set subsampling for yCoord
			for (int k = xCoord; k < xCoord + rate; k++) {
				for (int l = yCoord; l < yCoord + rate; l++) {
					if (k < widthImage && l < heightBi)
						bi.setRGB(k, l, rgbY);
				}
			}
			// get to next yCoord jump over sampled Coords
			yCoord += rate;
		}
		return bi;
	}

		

	@Override
	public String[] mandatoryProperties() {
		return new String [] { "rate:n:1-8:2" };
	}
	
	@Override
	public String toString() {
		return "subsampling";
	}

}
