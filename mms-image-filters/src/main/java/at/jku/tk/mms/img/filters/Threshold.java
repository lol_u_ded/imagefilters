package at.jku.tk.mms.img.filters;

import java.awt.Color;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.FilteredImageSource;
import java.awt.image.RGBImageFilter;
import java.util.Properties;

import at.jku.tk.mms.img.FilterInterface;
import at.jku.tk.mms.img.pixels.Pixel;

/** Filter that implements image thresholding */
public class Threshold implements FilterInterface {


	@Override
	public Image runFilter(BufferedImage image, Properties settings) {
		int threshold = Integer.parseInt(settings.getProperty("threshold"));
		BufferedImage blackWhite = new BufferedImage(image.getColorModel(), image.copyData(null), image.isAlphaPremultiplied(),null);

		int height = image.getHeight();
		int width = image.getWidth();
		
		// for loop that iterates through each pixel of the image
		for (int runH = 0; runH<height; runH++) {
			
			for(int runW = 0; runW<width; runW++) {
		
				Color c = new Color(image.getRGB(runW,runH));
				
				// greyscale addition
				int red = (int) (c.getRed() * 0.299);
				int green =(int) (c.getGreen() * 0.587);
				int blue = (int) (c.getBlue() *0.114);
		
				int grey = red+green+blue;
				
				// check & set the colors to Black or White
				if(grey > threshold) {
					blackWhite.setRGB(runW,runH,Color.BLACK.getRGB());
				}else{
					blackWhite.setRGB(runW,runH,Color.WHITE.getRGB());
				}
			}
		}
		return blackWhite;
	}

	@Override
	public String[] mandatoryProperties() {
		return new String[] { "threshold:n:0-255:128" };
	}

	@Override
	public String toString() {
		return "Threshold Filter";
	}

}
