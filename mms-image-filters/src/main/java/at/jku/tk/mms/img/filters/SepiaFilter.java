package at.jku.tk.mms.img.filters;

import java.awt.Color;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.FilteredImageSource;
import java.awt.image.RGBImageFilter;
import java.util.Properties;

import at.jku.tk.mms.img.FilterInterface;
import at.jku.tk.mms.img.pixels.Pixel;

/** Just to show correct handles in ComboBox */
public class SepiaFilter implements FilterInterface{


	@Override
	public Image runFilter(BufferedImage image, Properties settings) {
		
		BufferedImage sepiaImage = new BufferedImage(image.getColorModel(), image.copyData(null), image.isAlphaPremultiplied(),null);
		
		int height = image.getHeight();
		int width = image.getWidth();
		
		// for loop that iterates through each pixel of the image
		for (int runH = 0; runH<height; runH++) {
			
			for(int runW = 0; runW<width; runW++) {
		
				Color c = new Color(image.getRGB(runW,runH));
				
				// convert RGB to sepia with given formula
				int sepiaRed = (int) (
						(c.getRed()*.393) 
						+c.getGreen()*.769
						+c.getBlue()*.189);
							
				int sepiaGreen =(int) (
						(c.getRed()*.349) 
						+c.getGreen()*.686
						+c.getBlue()*.168);
				
				int sepiaBlue = (int) (
						(c.getRed()*.272) 
						+c.getGreen()*.534
						+c.getBlue()*.131);
		
				// check overflow
				if(sepiaRed > 255) { sepiaRed = 255;}
				if(sepiaGreen > 255) { sepiaGreen = 255;}
				if(sepiaBlue > 255) { sepiaBlue = 255;}
				
				// set color and create sepia-Image
				Color sepiaColor = new Color(sepiaRed,sepiaGreen,sepiaBlue);
				sepiaImage.setRGB(runW,runH,sepiaColor.getRGB());
			}
		}
		return sepiaImage;
	}

	@Override
	public String[] mandatoryProperties() {
		return new String[] { };
	}

	@Override
	public String toString() {
		return "Sepia Filter";
	}

}
